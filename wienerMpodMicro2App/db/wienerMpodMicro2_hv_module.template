#
#  Basic PVs for ISEG High Voltage module management
#
#  Macros required:
#     DEV   : base device name
#     HOST  : SNMP host
#     SLOT  : the zero based slot number 0..9
#
#

record(ai, "$(DEV):$(SLOT):ModuleLinesupply24VR")
{
  field(DESC, "Measured value of the line supply 24 V")
  field(DTYP, "Snmp")
  field(SCAN, "2 second")
  field(EGU,  "V")
  field(PREC, "3")
  field(INP,  "@$(HOST) public %(W)moduleAuxiliaryMeasurementVoltage0.ma$(SLOT) Float: 100 Fn")
}

record(ai, "$(DEV):$(SLOT):ModuleLinesupply5VR")
{
  field(DESC, "Measured value of the line supply 5 V")
  field(DTYP, "Snmp")
  field(SCAN, "2 second")
  field(EGU,  "V")
  field(PREC, "3")
  field(INP,  "@$(HOST) public %(W)moduleAuxiliaryMeasurementVoltage1.ma$(SLOT) Float: 100 Fn")
}

record(ai, "$(DEV):$(SLOT):ModuleHWVoltageLimitR")
{
  field(DESC, "Module hardware voltage limit")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(EGU,  "%")
  field(PREC, "2")
  field(INP,  "@$(HOST) public %(W)moduleHardwareLimitVoltage.ma$(SLOT) Float: 100 Fn")
}

record(ai, "$(DEV):$(SLOT):ModuleHWCurrentLimitR")
{
  field(DESC, "Module hardware current limit")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(EGU,  "%")
  field(PREC, "2")
  field(INP,  "@$(HOST) public %(W)moduleHardwareLimitCurrent.ma$(SLOT) Float: 100 Fn")
}

record(ai, "$(DEV):$(SLOT):ModuleRampSpeedVoltageR")
{
  field(DESC, "The setting of the voltage ramp in %")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(EGU,  "%")
  field(PREC, "2")
  field(INP,  "@$(HOST) public %(W)moduleRampSpeedVoltage.ma$(SLOT) Float: 100 Fn")
}

record(ao, "$(DEV):$(SLOT):ModuleRampSpeedVoltageS")
{
  field(DESC, "The setting of the voltage ramp in %")
  field(DTYP, "Snmp")
  field(EGU,  "%")
  field(PREC, "2")
  field(OUT,  "@$(HOST) guru %(W)moduleRampSpeedVoltage.ma$(SLOT) Float: 100 Fn")
}

record(ai, "$(DEV):$(SLOT):ModuleRampSpeedCurrentR")
{
  field(DESC, "The setting of the current ramp in %")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(EGU,  "%")
  field(PREC, "2")
  field(INP,  "@$(HOST) public %(W)moduleRampSpeedCurrent.ma$(SLOT) Float: 100 Fn")
}

record(ao, "$(DEV):$(SLOT):ModuleRampSpeedCurrentS")
{
  field(DESC, "The setting of the current ramp in %")
  field(DTYP, "Snmp")
  field(EGU,  "%")
  field(PREC, "2")
  field(OUT,  "@$(HOST) guru %(W)moduleRampSpeedCurrent.ma$(SLOT) Float: 100 Fn")
}

record(longin, "$(DEV):$(SLOT):iModuleStatusR")
{
  field(DESC, "The module status")
  field(DTYP, "Snmp")
  field(SCAN, ".5 second")
  field(INP,  "@$(HOST) public %(W)moduleStatus.ma$(SLOT) BITS: 255 ihr")
  field(FLNK, "$(DEV):$(SLOT):iModuleStatusBitsR")
}

record(mbbiDirect, "$(DEV):$(SLOT):iModuleStatusBitsR")
{
  field(INP,  "$(DEV):$(SLOT):iModuleStatusR MS")
  field(FLNK, "$(DEV):$(SLOT):iCalcModuleStatus")
}

record(bi, "$(DEV):$(SLOT):ModuleFineAdjustmentR")
{
  field(DESC, "Module has reached state fine adjustment")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B0 MS")
  field(ONAM, "Fine Adjusment")
}

record(bi, "$(DEV):$(SLOT):ModuleLiveInsertionR")
{
  field(DESC, "Module is in state live insertion")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B2 MS")
  field(ONAM, "Live-inserted")
}

#
# Not available/Not working
#  uncomment it's LNKx in "$(DEV):$(SLOT):iCalcModuleStatus"
#
#record(bi, "$(DEV):$(SLOT):ModuleHVOnR")
#{
#  field(DESC, "Is High voltage on")
#  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B3 MS")
#  field(ONAM, "HV ON")
#  field(ZNAM, "HV OFF")
#}

record(bi, "$(DEV):$(SLOT):ModuleNeedsServiceR")
{
  field(DESC, "Hardware failure detected")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B4 MS")
  field(ONAM, "Needs service")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleHWLimitVoltageGoodR")
{
  field(DESC, "Hardware limit voltage in proper range")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B5 MS")
  field(ONAM, "HW Limit Voltage OK")
  field(ZNAM, "HW Limit Voltage NOT OK")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleInputErrorR")
{
  field(DESC, "Input error with module access")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B6 MS")
  field(ONAM, "Input Error")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleChannelsNoErrorR")
{
  field(DESC, "All channels without any failure")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B8 MS")
  field(ONAM, "Channels are OK")
  field(ZNAM, "BAD CHANNEL(S)")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleChannelsStableR")
{
  field(DESC, "All channels stable, no ramp is active")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.B9 MS")
  field(ONAM, "Channels Stable")
  field(ZNAM, "Ramping")
}

record(bi, "$(DEV):$(SLOT):ModuleSafetyLoopR")
{
  field(DESC, "Safety loop is closed")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BA MS")
  field(ONAM, "Safety Loop is closed")
  field(ZNAM, "Safety Loop is OPEN")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventActiveR")
{
  field(DESC, "Any event is active and mask is set")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BB MS")
  field(ONAM, "Event active")
  field(ZNAM, "No events")
  field(OSV,  "MINOR")
}

record(bi, "$(DEV):$(SLOT):ModuleGoodR")
{
  field(DESC, "Module state is good")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BC MS")
  field(ONAM, "Good")
  field(ZNAM, "Module is BAD")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleSupplyGoodR")
{
  field(DESC, "Power supply is good")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BD MS")
  field(ONAM, "Power supply is good")
  field(ZNAM, "Power supply is NOT good")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleTemperatureGoodR")
{
  field(DESC, "Module temperature is good")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BE MS")
  field(ONAM, "Temperature is good")
  field(ZNAM, "Temperature is NOT good")
  field(ZSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleKillEnableR")
{
  field(DESC, "Module state of kill enable")
  field(INP,  "$(DEV):$(SLOT):iModuleStatusBitsR.BF MS")
  field(ONAM, "Kill Enabled")
}

record(fanout, "$(DEV):$(SLOT):iCalcModuleStatus")
{
  field(LNK1, "$(DEV):$(SLOT):ModuleFineAdjustmentR")
  field(LNK2, "$(DEV):$(SLOT):ModuleLiveInsertionR")
#  field(LNK3, "$(DEV):$(SLOT):ModuleHVOnR")
  field(LNK4, "$(DEV):$(SLOT):ModuleNeedsServiceR")
  field(LNK5, "$(DEV):$(SLOT):ModuleHWLimitVoltageGoodR")
  field(LNK6, "$(DEV):$(SLOT):ModuleInputErrorR")
  field(FLNK, "$(DEV):$(SLOT):iCalcModuleStatus2")
}
record(fanout, "$(DEV):$(SLOT):iCalcModuleStatus2")
{
  field(LNK1, "$(DEV):$(SLOT):ModuleChannelsNoErrorR")
  field(LNK2, "$(DEV):$(SLOT):ModuleChannelsStableR")
  field(LNK3, "$(DEV):$(SLOT):ModuleSafetyLoopR")
  field(LNK4, "$(DEV):$(SLOT):ModuleEventActiveR")
  field(LNK5, "$(DEV):$(SLOT):ModuleGoodR")
  field(LNK6, "$(DEV):$(SLOT):ModuleSupplyGoodR")
  field(FLNK, "$(DEV):$(SLOT):iCalcModuleStatus3")
}
record(fanout, "$(DEV):$(SLOT):iCalcModuleStatus3")
{
  field(LNK1, "$(DEV):$(SLOT):ModuleTemperatureGoodR")
  field(LNK2, "$(DEV):$(SLOT):ModuleKillEnableR")
}

record(longin, "$(DEV):$(SLOT):iModuleEventStatusR")
{
  field(DTYP, "Snmp")
  field(SCAN, "1 second")
  field(INP,  "@$(HOST) public %(W)moduleEventStatus.ma$(SLOT) BITS: 255 ihr")
  field(FLNK, "$(DEV):$(SLOT):iModuleEventStatusBitsR")
}

record(mbbiDirect, "$(DEV):$(SLOT):iModuleEventStatusBitsR")
{
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusR MS")
  field(FLNK, "$(DEV):$(SLOT):iCalcModuleEventStatus")
}

record(bi, "$(DEV):$(SLOT):ModuleEventPowerFailR")
{
  field(DESC, "Event power fail generated")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.B0 MS")
  field(ONAM, "Power fail")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventLiveInsertionR")
{
  field(DESC, "Event live insertion")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.B2 MS")
  field(ONAM, "Live insertion")
}

record(bi, "$(DEV):$(SLOT):ModuleEventServiceR")
{
  field(DESC, "Module event: Hardware failure detected")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.B4 MS")
  field(ONAM, "Service needed")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventHWVoltageLimitNotGoodR")
{
  field(DESC, "HW voltage limit is not in proper range")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.B5 MS")
  field(ONAM, "HW Voltage Limit NOT OK")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventInputErrorR")
{
  field(DESC, "Input error with module access")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.B6 MS")
  field(ONAM, "Input error")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventSafetyLoopNotGoodR")
{
  field(DESC, "Safety loop is open")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.BA MS")
  field(ONAM, "Safety Loop was OPEN")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventSupplyNotGoodR")
{
  field(DESC, "At least one of the supplies is not good")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.BD MS")
  field(ONAM, "Supply was BAD")
  field(OSV,  "MAJOR")
}

record(bi, "$(DEV):$(SLOT):ModuleEventTemperatureNotGoodR")
{
  field(DESC, "Temperature was above the threshold")
  field(INP,  "$(DEV):$(SLOT):iModuleEventStatusBitsR.BE MS")
  field(ONAM, "Temperature was not OK")
  field(OSV,  "MAJOR")
}

record(fanout, "$(DEV):$(SLOT):iCalcModuleEventStatus")
{
  field(LNK1, "$(DEV):$(SLOT):ModuleEventPowerFailR")
  field(LNK2, "$(DEV):$(SLOT):ModuleEventLiveInsertionR")
  field(LNK3, "$(DEV):$(SLOT):ModuleEventServiceR")
  field(LNK4, "$(DEV):$(SLOT):ModuleEventHWVoltageLimitNotGoodR")
  field(LNK5, "$(DEV):$(SLOT):ModuleEventInputErrorR")
  field(LNK6, "$(DEV):$(SLOT):ModuleEventSafetyLoopNotGoodR")
  field(FLNK, "$(DEV):$(SLOT):iCalcModuleEventStatus2")
}
record(fanout, "$(DEV):$(SLOT):iCalcModuleEventStatus2")
{
  field(LNK1, "$(DEV):$(SLOT):ModuleEventSupplyNotGoodR")
  field(LNK2, "$(DEV):$(SLOT):ModuleEventTemperatureNotGoodR")
}

record(longin, "$(DEV):$(SLOT):ModuleChannelEventStatusR")
{
  field(DESC, "Bit field with one bit for every channel")
  field(DTYP, "Snmp")
  field(SCAN, "1 second")
# According to the MIB, it is an INTEGER, but the crate sends back a Hex-STRING
#  field(INP,  "@$(HOST) public %(W)moduleEventChannelStatus.ma$(SLOT) INTEGER: 100")
  field(INP,  "@$(HOST) public %(W)moduleEventChannelStatus.ma$(SLOT) Hex-STRING: 100 ih")
}

record(ai, "$(DEV):$(SLOT):ModuleTemperatureR")
{
  field(DESC, "Measured module temperature of sensor 1")
  field(DTYP, "Snmp")
  field(SCAN, "2 second")
  field(EGU,  "C")
  field(PREC, "2")
  field(INP,  "@$(HOST) public %(W)moduleAuxiliaryMeasurementTemperature0.ma$(SLOT) Float: 100 Fn")
}

record(bo, "$(DEV):$(SLOT):ModuleClearEventsS")
{
  field(DESC, "Clear all events of the module")
  field(FLNK, "$(DEV):$(SLOT):iModuleClearEventsS")
  field(ONAM, "Clear")
  field(ZNAM, "")
  field(VAL,  "0")
}

record(longin, "$(DEV):$(SLOT):iOne")
{
  field(VAL,  "1")
  field(DISP, "1")
}
record(longout, "$(DEV):$(SLOT):iModuleClearEventsS")
{
  field(DTYP, "Snmp")
  field(DOL,  "$(DEV):$(SLOT):iOne")
  field(OMSL, "closed_loop")
  field(OUT,  "@$(HOST) guru %(W)moduleDoClear.ma$(SLOT) INTEGER: 100 i")
}

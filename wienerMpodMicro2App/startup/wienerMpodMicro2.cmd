# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require asyn, 4.33.0
require snmp, 1.0.0.2
require calc, 3.7.1
# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
# if you change this library version remember to update it at wienerStartupScriptGenerator.sh too;
require wienerMpodMicro2, 0.0.1

# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
# @field DEV
# @type STRING
# Base device name
epicsEnvSet(DEV, "LabS-Utgard-WienerMPOD")
# -----------------------------------------------------------------------------
# @field HOST
# @type STRING
# IP address of SNMP HOST
epicsEnvSet(HOST, "10.4.3.33")

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------
epicsEnvSet(GROUP,   "5")
epicsEnvSet(MIBDIRS, "/usr/share/snmp/mibs:$(wienerMpodMicro2_DIR)")
epicsEnvSet(W,       "WIENER-CRATE-MIB::")
epicsEnvSet(VPREC,   9)
epicsEnvSet(CPREC,   9)

# -----------------------------------------------------------------------------
# Loading databases based on dynamically generated startup script
# -----------------------------------------------------------------------------
< wienerMpodMicro2_installed_modules.cmd

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit

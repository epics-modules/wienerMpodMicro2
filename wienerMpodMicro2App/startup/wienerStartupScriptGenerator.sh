#!/bin/bash

# -----------------------------------------------------------------------------
# ESS
# -----------------------------------------------------------------------------
# Wiener MPOD - Power Supply
#
# Author: douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

CONST_IP_WIENER="10.4.3.33"
# replace $E3_REQUIRE_LOCATION with path returned by it after source desired EPICS environment
CONST_MIBS_PATH="/usr/share/snmp/mibs:$E3_REQUIRE_LOCATION/siteApps/wienerMpodMicro2/0.0.1"
CONST_MIB_WIENER="+WIENER-CRATE-MIB"
CONST_VARIABLES="Variables"

CONST_HV_MOD="iseg"
CONST_LV_MOD="WIENER"

CONST_CMD_FILE="wienerMpodMicro2_installed_modules.cmd"
CONST_SEPARATOR="# -----------------------------------------------------------------------------"

IFS=$'\n'       # make newlines the only separator

# -----------------------------------------------------------------------------
# Creating the specific EPICS startup script depending on which modules are 
# plugged to the wiener crate, using SNMP to get the information
# -----------------------------------------------------------------------------
echo $CONST_SEPARATOR > $CONST_CMD_FILE
echo '# ESS' >> $CONST_CMD_FILE
echo '# auto-generated file, do not modify it!' >> $CONST_CMD_FILE
echo $CONST_SEPARATOR >> $CONST_CMD_FILE
echo '# Loading Wiener crate databases' >> $CONST_CMD_FILE
echo $CONST_SEPARATOR >> $CONST_CMD_FILE
echo 'dbLoadRecords(wienerMpodMicro2_crate.template, "DEV=$(DEV), HOST=$(HOST)")' >> $CONST_CMD_FILE
echo 'dbLoadRecords(wienerMpodMicro2_modules.db,     "DEV=$(DEV), HOST=$(HOST)")' >> $CONST_CMD_FILE
echo 'dbLoadRecords(wienerMpodMicro2_groups.db,      "DEV=$(DEV), HOST=$(HOST)")' >> $CONST_CMD_FILE

# -----------------------------------------------------------------------------
# At first, checking if the Wiener MPOD crate is accessible, otherwie, loads only the common crate database above
# -----------------------------------------------------------------------------
ping -c 1 $CONST_IP_WIENER &>/dev/null
if [[ $? -eq 1 ]];
then
    echo 'Wiener is unreachable!'
    exit 1
fi

# -----------------------------------------------------------------------------
# Then checking if it is powered on, otherwise try to power it on
# -----------------------------------------------------------------------------
# Example of a returned info from such request:
#   $> snmpwalk -Cp -Oqv -v 2c -M /usr/share/snmp/mibs:/epics/base-3.15.5/require/3.0.4/siteApps/wienerMpodMicro2/0.0.1/mibs -m +WIENER-CRATE-MIB -c public 10.4.3.33 sysMainSwitch.0
#   on
#   Variables found: 1
# -----------------------------------------------------------------------------
#   $> snmpwalk -Cp -Oqv -v 2c -M /usr/share/snmp/mibs:/epics/base-3.15.5/require/3.0.4/siteApps/wienerMpodMicro2/0.0.1/mibs -m +WIENER-CRATE-MIB -c public 10.4.3.33 sysMainSwitch.0
#   off
#   Variables found: 1
# -----------------------------------------------------------------------------
wienerStatus=$(snmpwalk -Cp -Oqv -v 2c -M $CONST_MIBS_PATH -m $CONST_MIB_WIENER -c public $CONST_IP_WIENER sysMainSwitch.0 | awk 'FNR <= 1')
if [[ $wienerStatus == 'off' ]];
then
   snmpset -v 2c -M $CONST_MIBS_PATH -m $CONST_MIB_WIENER -c private $CONST_IP_WIENER sysMainSwitch.0 i 1
   # When it is powered on takes a while to load some boards...
   sleep 30
fi

# -----------------------------------------------------------------------------
# Then iterate over the conected modules
# -----------------------------------------------------------------------------
# Example of a returned info from such request:
#   $> snmpwalk -Cp -Oqv -v 2c -M /usr/share/snmp/mibs:/epics/base-3.15.5/require/3.0.4/siteApps/wienerMpodMicro2/0.0.1/mibs -m +WIENER-CRATE-MIB -c public 10.4.3.33 moduleIndex
#   ma4
#   ma5
#   Variables found: 2
# -----------------------------------------------------------------------------
for module in $(snmpwalk -Cp -Oqv -v 2c -M $CONST_MIBS_PATH -m $CONST_MIB_WIENER -c public $CONST_IP_WIENER moduleIndex);
do
    if [[ $module =~ $CONST_VARIABLES ]];
    then
        break
    else
        # ---------------------------------------------------------------------
        # For each module board, load its detailed information and depending on the 
        # module name decide to configure High or Low voltage databases
        # ---------------------------------------------------------------------
        # Example of a returned info from such request:
        #   $> snmpwalk -Cp -Oqv -v 2c -M /usr/share/snmp/mibs:/epics/base-3.15.5/require/3.0.4/siteApps/wienerMpodMicro2/0.0.1/mibs -m +WIENER-CRATE-MIB -c public 10.4.3.33 moduleDescription.5
        #   "iseg, E08C2, 16, 7800126, 04.56"
        #   Variables found: 1
        # ---------------------------------------------------------------------
        #   $ snmpwalk -Cp -Oqv -v 2c -M /usr/share/snmp/mibs:/epics/base-3.15.5/require/3.0.4/siteApps/wienerMpodMicro2/0.0.1/mibs -m +WIENER-CRATE-MIB -c public 10.4.3.33 moduleDescription.6
        #   "WIENER, MPV8016, 8, 8900"
        #   Variables found: 1
        # ---------------------------------------------------------------------
        for moduleDesc in $(snmpwalk -Cp -Oqv -v 2c -M $CONST_MIBS_PATH -m $CONST_MIB_WIENER -c public $CONST_IP_WIENER moduleDescription.$module);
        do
            if [[ $moduleDesc =~ $CONST_VARIABLES ]];
            then
                break
            else
                # Splitting fields in one array
                IFS=’,’ read -ra MODULE_DESC_FIELDS <<< "$moduleDesc"
                # Removing spaces from the number of channels
                #CHANNEL_NUM=$(echo ${MODULE_DESC_FIELDS[2]} | awk '$1=$1')
                printf -v CHANNEL_NUM "%02d" $(echo ${MODULE_DESC_FIELDS[2]} | awk '$1=$1' | sed 's/^0*//')

                if  [[ $moduleDesc =~ $CONST_HV_MOD ]];
                then
                    # ---------------------------------------------------------
                    # High-voltage modules need a common and a specific databases
                    # ---------------------------------------------------------
                    echo '' >> $CONST_CMD_FILE
                    echo $CONST_SEPARATOR >> $CONST_CMD_FILE
                    echo '# Loading Wiener HighVoltage databases' >> $CONST_CMD_FILE
                    echo $CONST_SEPARATOR >> $CONST_CMD_FILE
                    echo 'epicsEnvSet(SLOT, "'${module:2}'")' >> $CONST_CMD_FILE
                    echo 'dbLoadRecords(wienerMpodMicro2_hv_module.template, "DEV=$(DEV), HOST=$(HOST), SLOT=$(SLOT)")' >> $CONST_CMD_FILE
                    echo 'dbLoadRecords(wienerMpodMicro2_hv_'$CHANNEL_NUM'_channel.db,   "DEV=$(DEV), HOST=$(HOST), SLOT=, VPREC=$(VPREC), CPREC=$(CPREC)")' >> $CONST_CMD_FILE
                elif [[ $moduleDesc =~ $CONST_LV_MOD ]];
                then
                    # ---------------------------------------------------------
                    # Low-voltage modules need a specific database
                    # ---------------------------------------------------------
                    echo '' >> $CONST_CMD_FILE
                    echo $CONST_SEPARATOR >> $CONST_CMD_FILE
                    echo '# Loading Wiener LowVoltage databases' >> $CONST_CMD_FILE
                    echo $CONST_SEPARATOR >> $CONST_CMD_FILE
                    echo 'epicsEnvSet(SLOT, "'${module:2}'")' >> $CONST_CMD_FILE
                    echo 'dbLoadRecords(wienerMpodMicro2_lv_'$CHANNEL_NUM'_channel.db,   "DEV=$(DEV), HOST=$(HOST), SLOT=$(SLOT), VPREC=$(VPREC), CPREC=$(CPREC)")' >> $CONST_CMD_FILE
                fi
            fi
        done
    fi
done
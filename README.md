# wienerMpodMicro2

European Spallation Source ERIC Site-specific EPICS module: wienerMpodMicro2

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/Wiener+MPOD+power+supply)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/wienerMpodMicro2-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
